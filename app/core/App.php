<?php

class App
{

  //Defining the Starting Controller and its method
  protected $controller = 'home';
  protected $method = 'index';

  //Parameters to the functions (URL)
  protected $params = [];

  /* Default constructor for the App (starting point)*/
  public function __construct()
  {
      $url = $this->parseUrl();

      //print_r($url); // For debugging purposes

      //Checking if the controller exists and setting it if it does
      $file = '../app/controllers/' . $url[0] . '.php';
      if(file_exists($file))
      {
        $this->controller = $url[0];
        unset($url[0]);
      }
      else if ($url[0] === "index.php" )
      {}
       //Error Handling
      else {
        $error = '../app/controllers/error.php';
        require_once $error;
        $controller = new ErrorController();
        return false;
      }


      //Importing the set controller and its methods
      require_once '../app/controllers/' . $this->controller . '.php';

      $this->controller = new $this->controller;

      if(isset($url[1]))
      {
        if(method_exists($this->controller, $url[1]))
        {
          $this->method = $url[1];
          unset($url[1]);
        }
      }

      //Wrapping params
      $this->params = $url ? array_values($url) : []; //if exists then put to url otherwise set empty array

      call_user_func_array([$this->controller, $this->method], $this->params);
  }

  //Function for parsing url and ignoring / characters
  public function parseUrl()
  {
    if(isset($_GET['url']))
    {

      $url = $_GET['url'];
      $url = rtrim($url, '/');
      $url = filter_var($url, FILTER_SANITIZE_URL);
      $url = explode('/', $url);

      return $url;
    }

  }

}
?>
