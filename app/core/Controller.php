<?php

class Controller
{

  /*
  * Method for setting the controller
  */
  public function model($model) //protected
  {
    require_once '../app/models/' . $model . '.php';
    return new $model();
  }

  /*
  * Method for outputting the view in controller
  */
  public function view($view, $data = []) //Array for avoiding missing argument
  {
    require_once '../app/views/' . $view . '.php';
  }
}
 ?>
