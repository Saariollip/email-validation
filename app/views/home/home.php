<!DOCTYPE html>
<html>
  <head>
    <?php include ('partials/head.php'); ?>

    <!-- Fonts and icons-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">


    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Loading custom styles -->
   <style type="text/css">  <?php include '..\public\stylesheets\style.php'; ?>   </style>

  </head>
  <body>
    <div class="container" id="index_container">
        <div class="row">
            <div class="col-sm-8 col-md-8">
                <div class="panel panel-default">
                      <?php include ('partials/welcome.php'); ?>
                  </div>
              </div>
            </div>
        </div>

      <?php include 'partials/footer.php'; ?>

      <script type="text/javascript" src="..\public\javascripts/scripts.js"></script>
  </body>
  
</html>
