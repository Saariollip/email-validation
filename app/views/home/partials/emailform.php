<div class="panel-heading">Email</div>
<div class="panel-body">
      <form class="form-horizontal" role="form" method="POST" action="home/welcome/" >

          <div class="form-group">
              <label for="email" class="col-md-4 control-label">E-Mail Address</label>

              <div id="email_field" class="col-md-6 input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i>          </span>
                    <input id="email" type="email" class="form-control" name="email" required placeholder="Enter a valid email address">


              </div>
          </div>


            <div id="submit_button" class="form-group">
              <div class="col-md-9 col-md-offset-8">
               <button type="submit" class="btn btn-primary">
                   <i class="fa fa-btn fa-arrow-right"></i> Submit
               </button>
             </div>
           </div>

    </form>
  </div>
