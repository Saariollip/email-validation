### What is this repository for? ###

* Quick summary

	PHP+MySQL program in simple MVC -architecture, which takes the user's email, checks it and saves it to the database.
	There is no PageController/Routing system (aliasing) so URI's work like Controller/Method/Param1/.../ParamX


### How do I get set up? ###

* Summary of set up

	Just download the program into PHP environment (>5.6) and setup database

* Database configuration

	There is a SQL command how to create the database explained in database/DbTable.txt, so it has to be set first.

* How to run tests

	This project does not have any unit tests. There are only few commented lines in code which says Debugging purposes or testing purposes, but
	I have removed most of them, because of cleaner code.

